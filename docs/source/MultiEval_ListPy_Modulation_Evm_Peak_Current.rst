Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: