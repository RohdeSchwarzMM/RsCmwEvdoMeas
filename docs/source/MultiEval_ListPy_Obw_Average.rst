Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Average.Average
	:members:
	:undoc-members:
	:noindex: