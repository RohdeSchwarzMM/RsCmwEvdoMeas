Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Apilot_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: