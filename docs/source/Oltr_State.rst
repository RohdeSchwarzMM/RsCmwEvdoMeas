State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:OLTR:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:OLTR:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.Oltr_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.oltr.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Oltr_State_All.rst