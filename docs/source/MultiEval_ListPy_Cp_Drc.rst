Drc
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Drc.Drc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.drc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Drc_State.rst
	MultiEval_ListPy_Cp_Drc_Current.rst
	MultiEval_ListPy_Cp_Drc_Average.rst
	MultiEval_ListPy_Cp_Drc_Maximum.rst
	MultiEval_ListPy_Cp_Drc_Minimum.rst