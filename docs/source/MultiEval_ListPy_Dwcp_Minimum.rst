Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: