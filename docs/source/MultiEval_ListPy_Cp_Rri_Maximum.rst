Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Rri_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: