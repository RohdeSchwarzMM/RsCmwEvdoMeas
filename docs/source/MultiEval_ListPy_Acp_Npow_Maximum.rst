Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: