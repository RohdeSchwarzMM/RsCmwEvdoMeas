Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:ACP:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:ACP:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:ACP:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:ACP:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:ACP:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:ACP:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Acp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: