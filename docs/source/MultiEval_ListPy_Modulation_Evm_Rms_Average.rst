Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: