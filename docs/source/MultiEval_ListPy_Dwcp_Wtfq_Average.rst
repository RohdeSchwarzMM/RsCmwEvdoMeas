Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfq_.Average.Average
	:members:
	:undoc-members:
	:noindex: