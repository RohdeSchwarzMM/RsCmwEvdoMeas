Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Obw_.Average.Average
	:members:
	:undoc-members:
	:noindex: