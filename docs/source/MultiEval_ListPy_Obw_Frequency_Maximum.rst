Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: