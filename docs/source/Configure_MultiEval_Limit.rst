Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:IQOFfset
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:IQIMbalance
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CFERror
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:TTERror
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:WQUality
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MAXPower
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MINPower
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CDP
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CDE
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CP
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:DWCP

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:IQOFfset
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:IQIMbalance
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CFERror
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:TTERror
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:WQUality
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MAXPower
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MINPower
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CDP
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CDE
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:CP
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:DWCP



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit.Limit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Evm.rst
	Configure_MultiEval_Limit_Merr.rst
	Configure_MultiEval_Limit_Perr.rst
	Configure_MultiEval_Limit_Acp.rst
	Configure_MultiEval_Limit_Obw.rst