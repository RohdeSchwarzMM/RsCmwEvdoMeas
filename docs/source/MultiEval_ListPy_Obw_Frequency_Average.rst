Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Average.Average
	:members:
	:undoc-members:
	:noindex: