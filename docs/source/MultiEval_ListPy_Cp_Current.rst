Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Current.Current
	:members:
	:undoc-members:
	:noindex: