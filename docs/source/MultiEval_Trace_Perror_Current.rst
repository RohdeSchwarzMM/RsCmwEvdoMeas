Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Perror_.Current.Current
	:members:
	:undoc-members:
	:noindex: