Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Adsc_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: