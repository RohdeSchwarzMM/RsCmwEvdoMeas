Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:RELative



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Current_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: