StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: