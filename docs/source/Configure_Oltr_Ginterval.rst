Ginterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:GINTerval:TIME
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:GINTerval

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:OLTR:GINTerval:TIME
	CONFigure:EVDO:MEASurement<Instance>:OLTR:GINTerval



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.Oltr_.Ginterval.Ginterval
	:members:
	:undoc-members:
	:noindex: