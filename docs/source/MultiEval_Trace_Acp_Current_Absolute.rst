Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:ABSolute
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:ABSolute
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:ABSolute

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:ABSolute
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:ABSolute
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:CURRent:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Current_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: