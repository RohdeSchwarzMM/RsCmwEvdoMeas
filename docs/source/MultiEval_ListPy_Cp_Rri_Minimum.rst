Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Rri_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: