Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Average.Average
	:members:
	:undoc-members:
	:noindex: