Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Rri_.Current.Current
	:members:
	:undoc-members:
	:noindex: