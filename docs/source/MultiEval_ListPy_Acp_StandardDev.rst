StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: