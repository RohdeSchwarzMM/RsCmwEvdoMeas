Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:WQUality:PMIN:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Wquality_.Pmin_.Current.Current
	:members:
	:undoc-members:
	:noindex: