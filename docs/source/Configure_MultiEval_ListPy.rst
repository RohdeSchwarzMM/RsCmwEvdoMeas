ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:COUNt
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:COUNt
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_SingleCmw.rst
	Configure_MultiEval_ListPy_Segment.rst