Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: