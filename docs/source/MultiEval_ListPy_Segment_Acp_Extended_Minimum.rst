Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Acp_.Extended_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: