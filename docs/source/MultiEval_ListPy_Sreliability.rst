Sreliability
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SRELiability
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SRELiability

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SRELiability
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SRELiability



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Sreliability.Sreliability
	:members:
	:undoc-members:
	:noindex: