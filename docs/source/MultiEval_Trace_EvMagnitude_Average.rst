Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Average.Average
	:members:
	:undoc-members:
	:noindex: