Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:ABSolute
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:ABSolute
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:ABSolute

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:ABSolute
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:ABSolute
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Maximum_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: