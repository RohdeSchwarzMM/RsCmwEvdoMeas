Cp
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Cp.Cp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.cp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Cp_Current.rst
	MultiEval_Cp_Average.rst
	MultiEval_Cp_Maximum.rst
	MultiEval_Cp_Minimum.rst