Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Merror_.Current.Current
	:members:
	:undoc-members:
	:noindex: