State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	single: FETCh:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	single: CALCulate:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	FETCh:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe
	CALCulate:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:DOWN:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.Oltr_.Sequence_.Trace_.Down_.State.State
	:members:
	:undoc-members:
	:noindex: