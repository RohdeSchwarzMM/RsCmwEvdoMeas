Cp
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp.Cp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Rri.rst
	MultiEval_ListPy_Cp_Pilot.rst
	MultiEval_ListPy_Cp_Adsc.rst
	MultiEval_ListPy_Cp_Apilot.rst
	MultiEval_ListPy_Cp_Drc.rst
	MultiEval_ListPy_Cp_Data.rst
	MultiEval_ListPy_Cp_Current.rst
	MultiEval_ListPy_Cp_Average.rst
	MultiEval_ListPy_Cp_Maximum.rst
	MultiEval_ListPy_Cp_Minimum.rst
	MultiEval_ListPy_Cp_State.rst