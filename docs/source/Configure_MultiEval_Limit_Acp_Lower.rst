Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:LOWer:RELative
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:LOWer:ABSolute

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:LOWer:RELative
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:LOWer:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Acp_.Lower.Lower
	:members:
	:undoc-members:
	:noindex: