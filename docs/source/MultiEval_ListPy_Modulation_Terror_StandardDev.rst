StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: