Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:RRI:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Rri_.Average.Average
	:members:
	:undoc-members:
	:noindex: