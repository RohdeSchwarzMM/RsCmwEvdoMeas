Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wotq_.Average.Average
	:members:
	:undoc-members:
	:noindex: