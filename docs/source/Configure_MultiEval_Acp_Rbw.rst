Rbw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:RBW:LOWer
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:RBW:UPPer

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:RBW:LOWer
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:RBW:UPPer



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Acp_.Rbw.Rbw
	:members:
	:undoc-members:
	:noindex: