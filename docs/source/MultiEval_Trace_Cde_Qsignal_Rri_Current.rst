Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Rri_.Current.Current
	:members:
	:undoc-members:
	:noindex: