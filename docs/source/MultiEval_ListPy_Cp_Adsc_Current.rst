Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Adsc_.Current.Current
	:members:
	:undoc-members:
	:noindex: