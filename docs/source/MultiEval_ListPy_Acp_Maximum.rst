Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: