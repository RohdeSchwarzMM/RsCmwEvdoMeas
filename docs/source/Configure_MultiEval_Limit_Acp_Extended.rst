Extended
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Acp_.Extended.Extended
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.acp.extended.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Acp_Extended_Lower.rst
	Configure_MultiEval_Limit_Acp_Extended_Upper.rst