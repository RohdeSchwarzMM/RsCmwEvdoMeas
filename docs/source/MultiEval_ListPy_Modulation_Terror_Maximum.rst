Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: