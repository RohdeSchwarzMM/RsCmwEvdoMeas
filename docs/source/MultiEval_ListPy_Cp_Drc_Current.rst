Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Drc_.Current.Current
	:members:
	:undoc-members:
	:noindex: