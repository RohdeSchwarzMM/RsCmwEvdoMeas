Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfi_.Current.Current
	:members:
	:undoc-members:
	:noindex: