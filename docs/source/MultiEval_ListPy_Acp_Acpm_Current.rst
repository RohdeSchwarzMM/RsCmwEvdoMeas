Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPM<AcpMinus>:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpm_.Current.Current
	:members:
	:undoc-members:
	:noindex: