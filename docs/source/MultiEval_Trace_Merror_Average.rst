Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Merror_.Average.Average
	:members:
	:undoc-members:
	:noindex: