Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: