Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Average.Average
	:members:
	:undoc-members:
	:noindex: