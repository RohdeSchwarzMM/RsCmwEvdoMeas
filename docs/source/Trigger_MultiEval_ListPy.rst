ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:LIST:MODE

.. code-block:: python

	TRIGger:EVDO:MEASurement<Instance>:MEValuation:LIST:MODE



.. autoclass:: RsCmwEvdoMeas.Implementations.Trigger_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex: