Pstep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:PSTep:DIRection
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:PSTep

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:OLTR:PSTep:DIRection
	CONFigure:EVDO:MEASurement<Instance>:OLTR:PSTep



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.Oltr_.Pstep.Pstep
	:members:
	:undoc-members:
	:noindex: