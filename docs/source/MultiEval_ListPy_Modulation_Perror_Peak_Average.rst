Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Peak_.Average.Average
	:members:
	:undoc-members:
	:noindex: