Down
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN
	single: FETCh:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN
	FETCh:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:DOWN



.. autoclass:: RsCmwEvdoMeas.Implementations.RpInterval_.Sequence_.Trace_.Down.Down
	:members:
	:undoc-members:
	:noindex: