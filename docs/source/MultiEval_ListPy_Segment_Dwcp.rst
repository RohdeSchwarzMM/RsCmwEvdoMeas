Dwcp
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Dwcp.Dwcp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.dwcp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Dwcp_Current.rst
	MultiEval_ListPy_Segment_Dwcp_Average.rst
	MultiEval_ListPy_Segment_Dwcp_Maximum.rst
	MultiEval_ListPy_Segment_Dwcp_Minimum.rst