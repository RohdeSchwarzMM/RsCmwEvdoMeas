Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Pilot_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: