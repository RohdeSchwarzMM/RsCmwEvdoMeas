Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wotq_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: