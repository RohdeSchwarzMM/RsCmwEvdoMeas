Pilot
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Pilot.Pilot
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.pilot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Pilot_State.rst
	MultiEval_ListPy_Cp_Pilot_Current.rst
	MultiEval_ListPy_Cp_Pilot_Average.rst
	MultiEval_ListPy_Cp_Pilot_Maximum.rst
	MultiEval_ListPy_Cp_Pilot_Minimum.rst