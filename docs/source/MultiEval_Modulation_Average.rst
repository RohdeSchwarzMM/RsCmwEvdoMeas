Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: