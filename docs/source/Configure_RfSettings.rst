RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:CHANnel
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:EVDO:MEASurement<Instance>:RFSettings:BCLass

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:EVDO:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:EVDO:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:EVDO:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:EVDO:MEASurement<Instance>:RFSettings:CHANnel
	CONFigure:EVDO:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:EVDO:MEASurement<Instance>:RFSettings:BCLass



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex: