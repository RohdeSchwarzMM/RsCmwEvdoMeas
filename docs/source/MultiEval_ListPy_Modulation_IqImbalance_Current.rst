Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Current.Current
	:members:
	:undoc-members:
	:noindex: