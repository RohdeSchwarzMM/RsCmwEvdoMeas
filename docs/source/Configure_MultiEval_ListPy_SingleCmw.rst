SingleCmw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.ListPy_.SingleCmw.SingleCmw
	:members:
	:undoc-members:
	:noindex: