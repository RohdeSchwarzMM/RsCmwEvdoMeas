Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Drc_.Average.Average
	:members:
	:undoc-members:
	:noindex: