Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Pilot_.Average.Average
	:members:
	:undoc-members:
	:noindex: