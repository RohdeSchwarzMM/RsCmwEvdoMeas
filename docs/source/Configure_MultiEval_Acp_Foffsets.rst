Foffsets
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:FOFFsets:LOWer
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:FOFFsets:UPPer

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:FOFFsets:LOWer
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:FOFFsets:UPPer



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Acp_.Foffsets.Foffsets
	:members:
	:undoc-members:
	:noindex: