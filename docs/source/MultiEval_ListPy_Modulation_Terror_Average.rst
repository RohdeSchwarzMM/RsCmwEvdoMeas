Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror_.Average.Average
	:members:
	:undoc-members:
	:noindex: