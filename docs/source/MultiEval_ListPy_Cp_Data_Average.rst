Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Data_.Average.Average
	:members:
	:undoc-members:
	:noindex: