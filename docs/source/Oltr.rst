Oltr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:EVDO:MEASurement<Instance>:OLTR
	single: ABORt:EVDO:MEASurement<Instance>:OLTR
	single: STOP:EVDO:MEASurement<Instance>:OLTR

.. code-block:: python

	INITiate:EVDO:MEASurement<Instance>:OLTR
	ABORt:EVDO:MEASurement<Instance>:OLTR
	STOP:EVDO:MEASurement<Instance>:OLTR



.. autoclass:: RsCmwEvdoMeas.Implementations.Oltr.Oltr
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.oltr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Oltr_State.rst
	Oltr_Sequence.rst