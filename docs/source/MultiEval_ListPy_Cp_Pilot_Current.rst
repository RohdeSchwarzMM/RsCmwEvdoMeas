Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Pilot_.Current.Current
	:members:
	:undoc-members:
	:noindex: