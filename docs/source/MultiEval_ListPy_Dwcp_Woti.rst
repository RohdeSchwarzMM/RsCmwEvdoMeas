Woti
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Woti.Woti
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.dwcp.woti.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Dwcp_Woti_Current.rst
	MultiEval_ListPy_Dwcp_Woti_Average.rst
	MultiEval_ListPy_Dwcp_Woti_Maximum.rst
	MultiEval_ListPy_Dwcp_Woti_Minimum.rst