Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: