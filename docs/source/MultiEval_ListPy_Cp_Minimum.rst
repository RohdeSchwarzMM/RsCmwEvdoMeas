Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: