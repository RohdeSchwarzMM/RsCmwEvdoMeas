Oltr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:TOUT
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:REPetition
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:MOEXception
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:SEQuence

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:OLTR:TOUT
	CONFigure:EVDO:MEASurement<Instance>:OLTR:REPetition
	CONFigure:EVDO:MEASurement<Instance>:OLTR:MOEXception
	CONFigure:EVDO:MEASurement<Instance>:OLTR:SEQuence



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.Oltr.Oltr
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.oltr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Oltr_Pstep.rst
	Configure_Oltr_RpInterval.rst
	Configure_Oltr_Ginterval.rst
	Configure_Oltr_Limit.rst