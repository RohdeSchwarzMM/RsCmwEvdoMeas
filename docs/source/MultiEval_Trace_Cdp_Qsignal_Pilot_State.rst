State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Pilot_.State.State
	:members:
	:undoc-members:
	:noindex: