StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: