Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Adsc_.Average.Average
	:members:
	:undoc-members:
	:noindex: