Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: