Dwcp
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp.Dwcp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.dwcp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Dwcp_Wtfi.rst
	MultiEval_ListPy_Dwcp_Wtfq.rst
	MultiEval_ListPy_Dwcp_Woti.rst
	MultiEval_ListPy_Dwcp_Wotq.rst
	MultiEval_ListPy_Dwcp_Current.rst
	MultiEval_ListPy_Dwcp_Average.rst
	MultiEval_ListPy_Dwcp_Maximum.rst
	MultiEval_ListPy_Dwcp_Minimum.rst