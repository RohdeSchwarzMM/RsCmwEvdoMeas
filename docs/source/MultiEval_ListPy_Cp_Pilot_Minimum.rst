Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Pilot_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: