Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpp_.Current.Current
	:members:
	:undoc-members:
	:noindex: