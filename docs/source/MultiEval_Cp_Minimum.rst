Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:CP:MINimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:MINimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:CP:MINimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Cp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: