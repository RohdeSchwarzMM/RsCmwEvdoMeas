Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: