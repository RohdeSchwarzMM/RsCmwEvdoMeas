Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfq_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: