Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:OBW<Obw>:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:OBW<Obw>:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:OBW<Obw>:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:OBW<Obw>:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:OBW<Obw>:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:OBW<Obw>:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Obw_.Current.Current
	:members:
	:undoc-members:
	:noindex: