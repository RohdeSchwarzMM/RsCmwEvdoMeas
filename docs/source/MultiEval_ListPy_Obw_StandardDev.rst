StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: