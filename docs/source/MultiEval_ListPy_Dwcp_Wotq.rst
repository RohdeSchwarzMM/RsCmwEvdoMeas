Wotq
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wotq.Wotq
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.dwcp.wotq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Dwcp_Wotq_Current.rst
	MultiEval_ListPy_Dwcp_Wotq_Average.rst
	MultiEval_ListPy_Dwcp_Wotq_Maximum.rst
	MultiEval_ListPy_Dwcp_Wotq_Minimum.rst