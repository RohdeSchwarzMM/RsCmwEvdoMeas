Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr200
	rc = driver.multiEval.listPy.segment.repcap_segment_get()
	driver.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment.Segment
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Modulation.rst
	MultiEval_ListPy_Segment_Cp.rst
	MultiEval_ListPy_Segment_Dwcp.rst
	MultiEval_ListPy_Segment_Acp.rst
	MultiEval_ListPy_Segment_Obw.rst