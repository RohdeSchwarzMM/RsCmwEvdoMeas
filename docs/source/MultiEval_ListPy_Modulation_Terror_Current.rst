Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror_.Current.Current
	:members:
	:undoc-members:
	:noindex: