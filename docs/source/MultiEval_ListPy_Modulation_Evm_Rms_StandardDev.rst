StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: