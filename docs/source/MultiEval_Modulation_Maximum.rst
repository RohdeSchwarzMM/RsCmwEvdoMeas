Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Modulation_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: