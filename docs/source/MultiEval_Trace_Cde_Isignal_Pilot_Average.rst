Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Pilot_.Average.Average
	:members:
	:undoc-members:
	:noindex: