Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Woti_.Current.Current
	:members:
	:undoc-members:
	:noindex: