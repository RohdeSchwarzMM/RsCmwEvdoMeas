Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:OBW<Obw>:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Obw_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: