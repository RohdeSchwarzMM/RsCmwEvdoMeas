Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:UPPer

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:UPPer



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Upper.Upper
	:members:
	:undoc-members:
	:noindex: