Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:CP:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:CP:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Cp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: