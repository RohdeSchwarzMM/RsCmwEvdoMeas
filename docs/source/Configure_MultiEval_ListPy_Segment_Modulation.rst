Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex: