Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Current.Current
	:members:
	:undoc-members:
	:noindex: