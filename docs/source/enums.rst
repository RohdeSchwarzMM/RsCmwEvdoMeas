Enums
=========

AckDsc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AckDsc.ACK
	# All values (4x):
	ACK | DNCare | DSC | OFF

BandClass
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandClass.AWS
	# Last value:
	value = enums.BandClass.USPC
	# All values (22x):
	AWS | B18M | IEXT | IM2K | JTAC | KCEL | KPCS | LBANd
	LO7C | N45T | NA7C | NA8S | NA9C | NAPC | PA4M | PA8M
	PS7C | SBANd | TACS | U25B | USC | USPC

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

Dmodulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dmodulation.AUTO
	# All values (6x):
	AUTO | B4 | E4E2 | Q2 | Q4 | Q4Q2

HalfSlot
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HalfSlot.BHSLots
	# All values (3x):
	BHSLots | FHSLot | SHSLot

MeasCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasCondition.DNCare
	# All values (3x):
	DNCare | OFF | ON

ObwUsedLimitSet
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ObwUsedLimitSet.SETA
	# All values (2x):
	SETA | SETB

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PlSubtype
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PlSubtype.ST01
	# All values (3x):
	ST01 | ST2 | ST3

Rbw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Rbw.F100k
	# Last value:
	value = enums.Rbw.F6K25
	# All values (10x):
	F100k | F10K | F12K5 | F1K0 | F1M0 | F1M23 | F25K | F30K
	F50K | F6K25

RefPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefPowerMode.ATPower
	# All values (2x):
	ATPower | PPOWer

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStateA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultStateA.ACTive
	# All values (3x):
	ACTive | IACTive | INVisible

ResultStateB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultStateB.ACTive
	# All values (3x):
	ACTive | INACtive | NAV

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerMode.ONCE
	# All values (2x):
	ONCE | SEGMent

RetriggerOption
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerOption.IFPower
	# All values (4x):
	IFPower | IFPSync | OFF | ON

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

Srate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Srate.SF16
	# All values (2x):
	SF16 | SF32

StatePower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StatePower.BOTH
	# All values (4x):
	BOTH | LOWer | OK | UPPer

StopConditionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopConditionB.NONE
	# All values (2x):
	NONE | OLFail

Tab
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Tab.MEVA
	# All values (2x):
	MEVA | OLTR

TestScenarioB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestScenarioB.CSPath
	# All values (4x):
	CSPath | MAPRotocol | SALone | UNDefined

TriggerSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSlope.FEDGe
	# All values (4x):
	FEDGe | OFF | ON | REDGe

UpDownDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpDownDirection.DOWN
	# All values (2x):
	DOWN | UP

WbFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WbFilter.F16M0
	# All values (2x):
	F16M0 | F8M0

