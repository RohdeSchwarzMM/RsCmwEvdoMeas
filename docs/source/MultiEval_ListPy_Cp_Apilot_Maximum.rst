Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Apilot_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: