Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.PnBand_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: