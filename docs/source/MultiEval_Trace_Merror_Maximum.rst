Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Merror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: