Wtfi
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfi.Wtfi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.dwcp.wtfi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Dwcp_Wtfi_Current.rst
	MultiEval_ListPy_Dwcp_Wtfi_Average.rst
	MultiEval_ListPy_Dwcp_Wtfi_Maximum.rst
	MultiEval_ListPy_Dwcp_Wtfi_Minimum.rst