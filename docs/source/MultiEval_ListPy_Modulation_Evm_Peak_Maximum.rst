Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: