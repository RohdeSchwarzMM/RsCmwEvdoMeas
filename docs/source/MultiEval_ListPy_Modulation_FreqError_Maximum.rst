Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.FreqError_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: