Obw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:MULTi
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:SETA
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:SETB

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:MULTi
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:SETA
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:SETB



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Obw.Obw
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Obw_Check.rst