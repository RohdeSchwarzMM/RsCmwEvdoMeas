Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Modulation_.Current.Current
	:members:
	:undoc-members:
	:noindex: