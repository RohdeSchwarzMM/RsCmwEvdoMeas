Wtfq
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfq.Wtfq
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.dwcp.wtfq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Dwcp_Wtfq_Current.rst
	MultiEval_ListPy_Dwcp_Wtfq_Average.rst
	MultiEval_ListPy_Dwcp_Wtfq_Maximum.rst
	MultiEval_ListPy_Dwcp_Wtfq_Minimum.rst