Rri
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Rri.Rri
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cdp.qsignal.rri.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cdp_Qsignal_Rri_Current.rst
	MultiEval_Trace_Cdp_Qsignal_Rri_Average.rst
	MultiEval_Trace_Cdp_Qsignal_Rri_Maximum.rst
	MultiEval_Trace_Cdp_Qsignal_Rri_Minimum.rst
	MultiEval_Trace_Cdp_Qsignal_Rri_State.rst
	MultiEval_Trace_Cdp_Qsignal_Rri_Limit.rst