Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Average.Average
	:members:
	:undoc-members:
	:noindex: