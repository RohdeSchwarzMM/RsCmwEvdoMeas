Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: