Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: