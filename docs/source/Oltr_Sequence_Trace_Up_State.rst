State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	single: FETCh:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	single: CALCulate:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	FETCh:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe
	CALCulate:EVDO:MEASurement<Instance>:OLTR:SEQuence<Sequence>:TRACe:UP:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.Oltr_.Sequence_.Trace_.Up_.State.State
	:members:
	:undoc-members:
	:noindex: