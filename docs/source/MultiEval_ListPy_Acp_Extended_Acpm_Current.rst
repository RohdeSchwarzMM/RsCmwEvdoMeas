Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpm_.Current.Current
	:members:
	:undoc-members:
	:noindex: