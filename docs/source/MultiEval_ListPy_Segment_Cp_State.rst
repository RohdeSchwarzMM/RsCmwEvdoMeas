State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CP:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CP:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Cp_.State.State
	:members:
	:undoc-members:
	:noindex: