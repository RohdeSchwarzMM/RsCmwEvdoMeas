Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:DWCP:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:DWCP:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:DWCP:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:DWCP:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Dwcp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: