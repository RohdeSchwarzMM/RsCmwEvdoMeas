Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Current.Current
	:members:
	:undoc-members:
	:noindex: