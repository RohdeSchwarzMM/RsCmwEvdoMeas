Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Acp_.Current.Current
	:members:
	:undoc-members:
	:noindex: