Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DRC:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Drc_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: