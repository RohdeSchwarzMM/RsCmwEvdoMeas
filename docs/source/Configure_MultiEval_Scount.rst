Scount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Scount.Scount
	:members:
	:undoc-members:
	:noindex: