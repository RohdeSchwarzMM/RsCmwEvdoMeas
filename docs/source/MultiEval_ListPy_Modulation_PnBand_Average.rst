Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PNBand:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.PnBand_.Average.Average
	:members:
	:undoc-members:
	:noindex: