Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:SPECtrum:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Spectrum_.Current.Current
	:members:
	:undoc-members:
	:noindex: