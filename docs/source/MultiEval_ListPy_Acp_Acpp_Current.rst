Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpp_.Current.Current
	:members:
	:undoc-members:
	:noindex: