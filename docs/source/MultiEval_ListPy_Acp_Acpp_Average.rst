Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpp_.Average.Average
	:members:
	:undoc-members:
	:noindex: