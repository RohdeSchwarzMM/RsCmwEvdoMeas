Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: