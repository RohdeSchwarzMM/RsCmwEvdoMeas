Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Apilot_.Current.Current
	:members:
	:undoc-members:
	:noindex: