Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:LOWer:RELative
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:LOWer:ABSolute

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:LOWer:RELative
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:LOWer:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Acp_.Extended_.Lower.Lower
	:members:
	:undoc-members:
	:noindex: