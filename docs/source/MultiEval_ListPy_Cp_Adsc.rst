Adsc
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Adsc.Adsc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.adsc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Adsc_State.rst
	MultiEval_ListPy_Cp_Adsc_Current.rst
	MultiEval_ListPy_Cp_Adsc_Average.rst
	MultiEval_ListPy_Cp_Adsc_Maximum.rst
	MultiEval_ListPy_Cp_Adsc_Minimum.rst