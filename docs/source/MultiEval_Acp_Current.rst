Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:ACP:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:ACP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:ACP:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:ACP:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:ACP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:ACP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Acp_.Current.Current
	:members:
	:undoc-members:
	:noindex: