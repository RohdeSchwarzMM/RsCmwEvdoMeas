Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:MINimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:MINimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:MINimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Modulation_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: