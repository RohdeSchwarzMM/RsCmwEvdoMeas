Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:CP:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:CP:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Cp_.Average.Average
	:members:
	:undoc-members:
	:noindex: