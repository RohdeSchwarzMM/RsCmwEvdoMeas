Setup
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Setup.Setup
	:members:
	:undoc-members:
	:noindex: