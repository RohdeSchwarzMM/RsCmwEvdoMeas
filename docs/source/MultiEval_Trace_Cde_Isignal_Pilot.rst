Pilot
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Pilot.Pilot
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cde.isignal.pilot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cde_Isignal_Pilot_Current.rst
	MultiEval_Trace_Cde_Isignal_Pilot_Average.rst
	MultiEval_Trace_Cde_Isignal_Pilot_Maximum.rst
	MultiEval_Trace_Cde_Isignal_Pilot_State.rst
	MultiEval_Trace_Cde_Isignal_Pilot_Limit.rst