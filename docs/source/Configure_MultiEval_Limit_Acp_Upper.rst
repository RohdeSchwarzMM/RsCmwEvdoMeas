Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:UPPer:RELative
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:UPPer:ABSolute

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:UPPer:RELative
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:UPPer:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Acp_.Upper.Upper
	:members:
	:undoc-members:
	:noindex: