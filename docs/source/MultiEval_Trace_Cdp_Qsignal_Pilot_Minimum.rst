Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:MINimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:MINimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:MINimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:PILot:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Pilot_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: