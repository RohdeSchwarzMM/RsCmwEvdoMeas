Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Rri_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: