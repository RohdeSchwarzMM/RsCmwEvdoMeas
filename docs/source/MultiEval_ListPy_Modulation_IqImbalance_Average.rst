Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Average.Average
	:members:
	:undoc-members:
	:noindex: