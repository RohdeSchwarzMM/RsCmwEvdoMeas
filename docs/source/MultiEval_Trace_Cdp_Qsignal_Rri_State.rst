State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Rri_.State.State
	:members:
	:undoc-members:
	:noindex: