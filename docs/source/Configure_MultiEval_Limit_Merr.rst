Merr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MERR:PEAK
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MERR:RMS

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MERR:PEAK
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:MERR:RMS



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Merr.Merr
	:members:
	:undoc-members:
	:noindex: