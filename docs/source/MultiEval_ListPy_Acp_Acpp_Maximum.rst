Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:ACPP<AcpPlus>:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Acpp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: