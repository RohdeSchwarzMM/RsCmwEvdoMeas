Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Rri_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: