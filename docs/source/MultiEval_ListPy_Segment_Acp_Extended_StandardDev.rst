StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:ACP:EXTended:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Acp_.Extended_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: