Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:EVDO:MEASurement<Instance>:SCENario:SALone
	single: ROUTe:EVDO:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:EVDO:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:EVDO:MEASurement<Instance>:SCENario:SALone
	ROUTe:EVDO:MEASurement<Instance>:SCENario:CSPath
	ROUTe:EVDO:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwEvdoMeas.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_MaProtocol.rst
	Route_Scenario_Catalog.rst