Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wotq_.Current.Current
	:members:
	:undoc-members:
	:noindex: