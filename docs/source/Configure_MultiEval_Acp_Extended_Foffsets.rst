Foffsets
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:FOFFsets:LOWer
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:FOFFsets:UPPer

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:FOFFsets:LOWer
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:FOFFsets:UPPer



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Acp_.Extended_.Foffsets.Foffsets
	:members:
	:undoc-members:
	:noindex: