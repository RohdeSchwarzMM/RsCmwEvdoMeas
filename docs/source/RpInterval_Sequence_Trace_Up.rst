Up
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP
	single: FETCh:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP
	FETCh:EVDO:MEASurement<Instance>:RPINterval:SEQuence<Sequence>:TRACe:UP



.. autoclass:: RsCmwEvdoMeas.Implementations.RpInterval_.Sequence_.Trace_.Up.Up
	:members:
	:undoc-members:
	:noindex: