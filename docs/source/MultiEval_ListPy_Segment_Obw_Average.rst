Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:OBW:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:OBW:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:OBW:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:OBW:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Segment_.Obw_.Average.Average
	:members:
	:undoc-members:
	:noindex: