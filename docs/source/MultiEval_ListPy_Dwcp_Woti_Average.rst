Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Woti_.Average.Average
	:members:
	:undoc-members:
	:noindex: