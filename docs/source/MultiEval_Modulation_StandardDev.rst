StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:SDEViation

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	FETCh:EVDO:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:MODulation:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Modulation_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: