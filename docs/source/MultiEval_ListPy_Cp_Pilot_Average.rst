Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:PILot:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Pilot_.Average.Average
	:members:
	:undoc-members:
	:noindex: