Rri
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Rri.Rri
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.rri.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Rri_State.rst
	MultiEval_ListPy_Cp_Rri_Current.rst
	MultiEval_ListPy_Cp_Rri_Average.rst
	MultiEval_ListPy_Cp_Rri_Maximum.rst
	MultiEval_ListPy_Cp_Rri_Minimum.rst