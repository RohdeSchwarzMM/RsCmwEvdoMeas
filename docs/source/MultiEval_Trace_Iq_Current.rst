Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Iq_.Current.Current
	:members:
	:undoc-members:
	:noindex: