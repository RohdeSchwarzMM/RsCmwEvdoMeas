Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpm_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: