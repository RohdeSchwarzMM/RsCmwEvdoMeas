Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Woti_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: