Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MINimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MINimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MINimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Pilot_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: