Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Pilot_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: