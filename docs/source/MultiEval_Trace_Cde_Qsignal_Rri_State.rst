State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Rri_.State.State
	:members:
	:undoc-members:
	:noindex: