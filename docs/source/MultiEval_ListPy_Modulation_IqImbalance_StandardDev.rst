StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: