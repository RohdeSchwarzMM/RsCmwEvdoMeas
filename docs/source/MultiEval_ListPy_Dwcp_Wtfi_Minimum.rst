Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfi_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: