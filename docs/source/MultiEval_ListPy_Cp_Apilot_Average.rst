Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:APILot:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Apilot_.Average.Average
	:members:
	:undoc-members:
	:noindex: