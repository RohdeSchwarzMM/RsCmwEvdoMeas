Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Wpow_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: