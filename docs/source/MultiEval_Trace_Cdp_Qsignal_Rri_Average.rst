Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:QSIGnal:RRI:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Qsignal_.Rri_.Average.Average
	:members:
	:undoc-members:
	:noindex: