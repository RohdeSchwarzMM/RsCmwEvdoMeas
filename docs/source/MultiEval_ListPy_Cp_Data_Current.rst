Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Data_.Current.Current
	:members:
	:undoc-members:
	:noindex: