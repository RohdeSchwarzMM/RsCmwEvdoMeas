Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:DISPlay

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:DISPlay



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings.rst
	Configure_MultiEval.rst
	Configure_Oltr.rst