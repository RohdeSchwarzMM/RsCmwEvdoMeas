Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Current.Current
	:members:
	:undoc-members:
	:noindex: