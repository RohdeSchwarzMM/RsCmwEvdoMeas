Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: