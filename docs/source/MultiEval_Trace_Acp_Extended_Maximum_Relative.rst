Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:MAXimum:RELative



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Maximum_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: