StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:WPOW:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Wpow_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: