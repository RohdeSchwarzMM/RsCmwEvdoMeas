Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MINimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MINimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MINimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:RRI:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Rri_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: