Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:NPOW:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Npow_.Average.Average
	:members:
	:undoc-members:
	:noindex: