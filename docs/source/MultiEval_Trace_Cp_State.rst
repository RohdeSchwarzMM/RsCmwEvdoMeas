State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cp_.State.State
	:members:
	:undoc-members:
	:noindex: