Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfi_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: