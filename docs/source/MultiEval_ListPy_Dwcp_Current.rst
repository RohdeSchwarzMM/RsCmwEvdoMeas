Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Current.Current
	:members:
	:undoc-members:
	:noindex: