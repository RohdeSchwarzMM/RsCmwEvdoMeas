Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:RELative
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:RELative
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:RELative

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:RELative
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:RELative
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:MAXimum:RELative



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Maximum_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: