Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:CP:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:CP:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:CP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:CP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Cp_.Current.Current
	:members:
	:undoc-members:
	:noindex: