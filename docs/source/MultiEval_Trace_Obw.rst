Obw<Obw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.trace.obw.repcap_obw_get()
	driver.multiEval.trace.obw.repcap_obw_set(repcap.Obw.Nr1)





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Obw.Obw
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Obw_Current.rst