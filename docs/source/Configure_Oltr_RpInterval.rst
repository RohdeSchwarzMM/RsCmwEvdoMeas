RpInterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:RPINterval:TIME
	single: CONFigure:EVDO:MEASurement<Instance>:OLTR:RPINterval

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:OLTR:RPINterval:TIME
	CONFigure:EVDO:MEASurement<Instance>:OLTR:RPINterval



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.Oltr_.RpInterval.RpInterval
	:members:
	:undoc-members:
	:noindex: