Spectrum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SPECtrum

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SPECtrum



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Spectrum.Spectrum
	:members:
	:undoc-members:
	:noindex: