State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:STATe

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:STATe



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Pilot_.State.State
	:members:
	:undoc-members:
	:noindex: