Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfq_.Current.Current
	:members:
	:undoc-members:
	:noindex: