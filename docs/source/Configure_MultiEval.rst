MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:DMODulation
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:HSLot
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:DRC
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACK
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACKDsc
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:DATA
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:APILot
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:PLSubtype
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:SFACtor
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ILCMask
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:QLCMask
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RPMode
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:IQLCheck

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:DMODulation
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:HSLot
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:DRC
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACK
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACKDsc
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:DATA
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:APILot
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:PLSubtype
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:SFACtor
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ILCMask
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:QLCMask
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RPMode
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:IQLCheck



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Scount.rst
	Configure_MultiEval_Carrier.rst
	Configure_MultiEval_Acp.rst
	Configure_MultiEval_Result.rst
	Configure_MultiEval_Limit.rst
	Configure_MultiEval_ListPy.rst