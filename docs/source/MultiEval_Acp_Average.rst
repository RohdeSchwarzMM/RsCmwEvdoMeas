Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:ACP:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:ACP:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:ACP:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:ACP:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:ACP:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:ACP:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Acp_.Average.Average
	:members:
	:undoc-members:
	:noindex: