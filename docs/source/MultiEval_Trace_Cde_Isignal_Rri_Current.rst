Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:RRI:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:RRI:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:RRI:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:RRI:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:RRI:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:RRI:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Rri_.Current.Current
	:members:
	:undoc-members:
	:noindex: