Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:SETTing
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:ENABle
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:SELect
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:FOFFset
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:FREQuency
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:WBFilter

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:SETTing
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:ENABle
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:SELect
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:FOFFset
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:FREQuency
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:CARRier:WBFilter



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: