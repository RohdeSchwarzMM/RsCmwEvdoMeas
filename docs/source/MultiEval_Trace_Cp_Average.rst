Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cp_.Average.Average
	:members:
	:undoc-members:
	:noindex: