Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPP<AcpPlus>:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: