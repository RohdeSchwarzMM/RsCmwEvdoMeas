Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:ADSC:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Adsc_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: