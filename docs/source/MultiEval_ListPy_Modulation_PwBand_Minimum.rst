Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PWBand:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.PwBand_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: