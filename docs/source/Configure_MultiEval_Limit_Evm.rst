Evm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:EVM:PEAK
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:EVM:RMS

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:EVM:PEAK
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:EVM:RMS



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Evm.Evm
	:members:
	:undoc-members:
	:noindex: