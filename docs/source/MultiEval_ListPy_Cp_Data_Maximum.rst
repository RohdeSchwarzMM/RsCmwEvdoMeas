Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Data_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: