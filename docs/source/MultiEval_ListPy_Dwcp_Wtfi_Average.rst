Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFI:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfi_.Average.Average
	:members:
	:undoc-members:
	:noindex: