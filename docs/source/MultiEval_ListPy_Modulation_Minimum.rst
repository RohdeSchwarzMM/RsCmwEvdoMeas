Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: