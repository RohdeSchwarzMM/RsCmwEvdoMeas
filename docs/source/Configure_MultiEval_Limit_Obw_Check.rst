Check
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:CHECk:USED
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:CHECk

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:CHECk:USED
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:OBW:CHECk



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Obw_.Check.Check
	:members:
	:undoc-members:
	:noindex: