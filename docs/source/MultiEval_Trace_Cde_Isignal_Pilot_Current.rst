Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:ISIGnal:PILot:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Isignal_.Pilot_.Current.Current
	:members:
	:undoc-members:
	:noindex: