Apilot
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Apilot.Apilot
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cp.apilot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Cp_Apilot_State.rst
	MultiEval_ListPy_Cp_Apilot_Current.rst
	MultiEval_ListPy_Cp_Apilot_Average.rst
	MultiEval_ListPy_Cp_Apilot_Maximum.rst
	MultiEval_ListPy_Cp_Apilot_Minimum.rst