MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:EVDO:MEASurement<Instance>:MEValuation
	single: ABORt:EVDO:MEASurement<Instance>:MEValuation
	single: STOP:EVDO:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:EVDO:MEASurement<Instance>:MEValuation
	ABORt:EVDO:MEASurement<Instance>:MEValuation
	STOP:EVDO:MEASurement<Instance>:MEValuation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_State.rst
	MultiEval_Trace.rst
	MultiEval_Modulation.rst
	MultiEval_Cp.rst
	MultiEval_Acp.rst
	MultiEval_Obw.rst
	MultiEval_ListPy.rst