Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cp_.Current.Current
	:members:
	:undoc-members:
	:noindex: