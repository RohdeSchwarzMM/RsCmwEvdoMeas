Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:CP:DATA:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Cp_.Data_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: