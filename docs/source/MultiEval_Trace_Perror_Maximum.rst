Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Perror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: