Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:RRI:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Rri_.Average.Average
	:members:
	:undoc-members:
	:noindex: