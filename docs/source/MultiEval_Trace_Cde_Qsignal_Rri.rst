Rri
----------------------------------------





.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Rri.Rri
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cde.qsignal.rri.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Cde_Qsignal_Rri_Current.rst
	MultiEval_Trace_Cde_Qsignal_Rri_Average.rst
	MultiEval_Trace_Cde_Qsignal_Rri_Maximum.rst
	MultiEval_Trace_Cde_Qsignal_Rri_State.rst
	MultiEval_Trace_Cde_Qsignal_Rri_Limit.rst