Rbw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:RBW:LOWer
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:RBW:UPPer

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:RBW:LOWer
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:ACP:EXTended:RBW:UPPer



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Acp_.Extended_.Rbw.Rbw
	:members:
	:undoc-members:
	:noindex: