Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:RELative



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Average_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: