Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:ACPM<AcpMinus>:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Acpm_.Average.Average
	:members:
	:undoc-members:
	:noindex: