Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTQ:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wotq_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: