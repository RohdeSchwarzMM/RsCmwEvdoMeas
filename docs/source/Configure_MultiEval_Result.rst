Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:ALL
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:CDP
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:CDE
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:ACP
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:POWer
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:TXM
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:CP
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:OBW
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:IQ

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:ALL
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:CDP
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:CDE
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:ACP
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:POWer
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:TXM
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:CP
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:OBW
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:RESult:IQ



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Result.Result
	:members:
	:undoc-members:
	:noindex: