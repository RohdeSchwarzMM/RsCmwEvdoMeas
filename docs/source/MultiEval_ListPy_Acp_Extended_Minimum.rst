Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:ACP:EXTended:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Acp_.Extended_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: