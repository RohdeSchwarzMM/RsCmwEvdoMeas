Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:AVERage:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Average_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: