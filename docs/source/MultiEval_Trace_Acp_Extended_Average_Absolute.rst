Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:AVERage:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Average_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: