Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MINimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MINimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MINimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cp_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: