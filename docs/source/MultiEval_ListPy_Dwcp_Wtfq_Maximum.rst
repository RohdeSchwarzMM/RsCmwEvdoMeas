Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MAXimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WTFQ:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Wtfq_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: