Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDP:ISIGnal:PILot:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cdp_.Isignal_.Pilot_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: