Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.SingleCmw_.Connector.Connector
	:members:
	:undoc-members:
	:noindex: