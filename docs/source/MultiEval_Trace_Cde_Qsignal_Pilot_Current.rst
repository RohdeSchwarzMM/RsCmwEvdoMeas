Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:CURRent
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:CURRent

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:CURRent
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CDE:QSIGnal:PILot:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cde_.Qsignal_.Pilot_.Current.Current
	:members:
	:undoc-members:
	:noindex: