Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:CURRent



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.Current.Current
	:members:
	:undoc-members:
	:noindex: