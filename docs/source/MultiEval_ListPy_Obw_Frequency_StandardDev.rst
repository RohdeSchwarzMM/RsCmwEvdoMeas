StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:OBW:FREQuency:SDEViation



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Obw_.Frequency_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: