Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Perror_.Average.Average
	:members:
	:undoc-members:
	:noindex: