MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:EVDO:MEASurement<Instance>:MEValuation:EOFFset

.. code-block:: python

	TRIGger:EVDO:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:EVDO:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:EVDO:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:EVDO:MEASurement<Instance>:MEValuation:DELay
	TRIGger:EVDO:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:EVDO:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:EVDO:MEASurement<Instance>:MEValuation:EOFFset



.. autoclass:: RsCmwEvdoMeas.Implementations.Trigger_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_MultiEval_Catalog.rst
	Trigger_MultiEval_ListPy.rst