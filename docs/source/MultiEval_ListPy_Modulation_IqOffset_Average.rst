Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqOffset_.Average.Average
	:members:
	:undoc-members:
	:noindex: