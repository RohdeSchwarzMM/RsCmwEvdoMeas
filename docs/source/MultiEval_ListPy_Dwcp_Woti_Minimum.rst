Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MINimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MINimum

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MINimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:DWCP:WOTI:MINimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Dwcp_.Woti_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: