Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage

.. code-block:: python

	FETCh:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: