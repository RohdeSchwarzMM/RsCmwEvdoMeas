Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:UPPer:RELative
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:UPPer:ABSolute

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:UPPer:RELative
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:ACP:EXTended:UPPer:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Acp_.Extended_.Upper.Upper
	:members:
	:undoc-members:
	:noindex: