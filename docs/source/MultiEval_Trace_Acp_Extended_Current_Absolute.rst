Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:ACP:EXTended:CURRent:ABSolute



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Acp_.Extended_.Current_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: