Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MAXimum
	single: FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MAXimum
	single: CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MAXimum

.. code-block:: python

	READ:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MAXimum
	FETCh:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MAXimum
	CALCulate:EVDO:MEASurement<Instance>:MEValuation:TRACe:CP:MAXimum



.. autoclass:: RsCmwEvdoMeas.Implementations.MultiEval_.Trace_.Cp_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: