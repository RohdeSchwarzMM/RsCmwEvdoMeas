Perr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:PERR:PEAK
	single: CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:PERR:RMS

.. code-block:: python

	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:PERR:PEAK
	CONFigure:EVDO:MEASurement<Instance>:MEValuation:LIMit:PERR:RMS



.. autoclass:: RsCmwEvdoMeas.Implementations.Configure_.MultiEval_.Limit_.Perr.Perr
	:members:
	:undoc-members:
	:noindex: